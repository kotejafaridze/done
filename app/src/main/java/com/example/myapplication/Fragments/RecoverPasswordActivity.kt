package com.example.myapplication.Fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth

class RecoverPasswordActivity: Fragment(R.layout.recover_password_activity) {
    private lateinit var editTextEmail: EditText
    private lateinit var buttonSendEmail: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editTextEmail = view.findViewById(R.id.editTextEmail2)
        buttonSendEmail = view.findViewById(R.id.buttonSendEmail)


        buttonSendEmail.setOnClickListener {
            val email = editTextEmail.text.toString()

            val controler = Navigation.findNavController(view)
            val action4 = RecoverPasswordActivityDirections.actionRecoverPasswordActivityToLoginActivity()
            if(email.isEmpty()){
                Toast.makeText(getActivity(), "please enter email", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        Toast.makeText(getActivity(), "check email", Toast.LENGTH_SHORT).show()
                        controler.navigate(action4)


                    }else{
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show()
                    }
                }

        }

    }
}
