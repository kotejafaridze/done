package com.example.myapplication.Fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity: Fragment(R.layout.register_activity) {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextRepeatPassword: EditText
    private lateinit var buttonSubmit: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        editTextEmail = view.findViewById(R.id.editTextEmail)
        editTextPassword = view.findViewById(R.id.editTextPassword)
        editTextRepeatPassword = view.findViewById(R.id.editTextRepeatPassword)
        buttonSubmit = view.findViewById(R.id.buttonSubmit)
        val controler = Navigation.findNavController(view)


        buttonSubmit.setOnClickListener {
            val action3 = RegisterActivityDirections.actionRegisterActivityToLoginActivity()

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val repeatpassword = editTextRepeatPassword.text.toString()

            if (email.isEmpty() || password.isEmpty() || repeatpassword.isEmpty()) {
                Toast.makeText(getActivity(), "შენ რა დანარჩენ გრაფებს მე მიტოვებ შესავსებად?", Toast.LENGTH_LONG)
                    .show()
                return@setOnClickListener
            }
            if (email.contains("@")) {
                if (password == repeatpassword) {
                    if (password.matches(".*[!@#$%^&*()].*".toRegex())){
                        if (password.length >= 9) {
                            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        Toast.makeText(getActivity(),
                                            "ყოჩაღ კაი ბიჭი ხარ შენ!",
                                            Toast.LENGTH_LONG).show()
                                        controler.navigate(action3)
                                    } else {
                                        Toast.makeText(getActivity(),
                                            "შეცდომა! თავიდან სცადე", Toast.LENGTH_SHORT).show()
                                    }
                                }
                        }else{
                            Toast.makeText(getActivity(),"პაროლი 9-ზე ნაკლები სიმბოლოსგან შედგება დროზე გაასწორე!", Toast.LENGTH_LONG).show()
                        }
                    }else{
                        Toast.makeText(getActivity(), "რით ვერ დაიმახსოვრე მარტო ასოები და ციფრები არ შეიძლება პაროლში!", Toast.LENGTH_LONG).show()
                    }
                } else{
                    Toast.makeText(getActivity(), "რა არის პაროლი უკვე დაგავიწყდა?", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(getActivity(), "იმეილი ძაღლუკის გარეშე გაგაიგია?", Toast.LENGTH_LONG).show()
            }

        }
    }



}