package com.example.myapplication.Fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth

class LoginActivity: Fragment(R.layout.login_activity) {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonLogin: Button
    private lateinit var buttonRegistration: Button
    private lateinit var buttonRecoverPassword: Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonRegistration= view.findViewById(R.id.buttonRegistration)
        editTextEmail = view.findViewById(R.id.editTextEmail)
        editTextPassword = view.findViewById(R.id.editTextPassword)
        buttonLogin = view.findViewById(R.id.buttonLogin)
        buttonRecoverPassword = view.findViewById(R.id.buttonResetPassword)

        val controler = Navigation.findNavController(view)
        buttonRegistration.setOnClickListener {
            val action = LoginActivityDirections.actionLoginActivityToRegisterActivity()
            controler.navigate(action)


        }
        buttonRecoverPassword.setOnClickListener {
            val action1 = LoginActivityDirections.actionLoginActivityToRecoverPasswordActivity()
            controler.navigate(action1)
        }
        buttonLogin.setOnClickListener {
            val action2 = LoginActivityDirections.actionLoginActivityToProfileActivity()
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            if(email.isEmpty() || password.isEmpty()){
                Toast.makeText(getActivity(), "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email,password)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        controler.navigate(action2)
                    }else{
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show()
                    }
                }
        }

    }
}