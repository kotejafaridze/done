package com.example.myapplication.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth

class ChangePasswordActivity: Fragment(R.layout.change_password_activity) {

    private lateinit var editTextNewPassword: EditText
    private lateinit var editTextConfirmpass: EditText
    private lateinit var buttonChangePassword: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        editTextNewPassword = view.findViewById(R.id.editTextNewPassword)
        buttonChangePassword = view.findViewById(R.id.buttonChangePassword)
        editTextConfirmpass = view.findViewById(R.id.editTextConfirmPass)
        val controler = Navigation.findNavController(view)
        buttonChangePassword.setOnClickListener {
            val action5 = ChangePasswordActivityDirections.actionChangePasswordActivityToProfileActivity()
            val password1 = editTextNewPassword.text.toString()
            val confirmpass1 =  editTextConfirmpass .text.toString()


            if ( password1.isEmpty() || confirmpass1.isEmpty()) {
                Toast.makeText(getActivity(), "შენ რა დანარჩენ გრაფებს მე მიტოვებ შესავსებად?", Toast.LENGTH_LONG)
                    .show()
                return@setOnClickListener
            }
            if (password1 == confirmpass1) {
                if(password1.matches(".*[!@#$%^&*()].*".toRegex())){
                    if (password1.length >= 8) {
                        FirebaseAuth.getInstance()?.currentUser?.updatePassword(password1)
                            ?.addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    Toast.makeText(getActivity(),
                                        "ყოჩაღ კაი ბიჭი ხარ შენ!",
                                        Toast.LENGTH_LONG).show()
                                    controler.navigate(action5)
                                } else {
                                    Toast.makeText(getActivity(),
                                        "შეცდომა! თავიდან სცადე", Toast.LENGTH_SHORT).show()
                                }
                            }
                    }else{
                        Toast.makeText(getActivity(),"პაროლი 8-ზე ნაკლები სიმბოლოსგან შედგება დროზე გაასწორე!", Toast.LENGTH_LONG).show()
                    }
                }else{
                    Toast.makeText(getActivity(), "რით ვერ დაიმახსოვრე მარტო ასოები და ციფრები არ შეიძლება პაროლში!", Toast.LENGTH_LONG).show()
                }
            } else{
                Toast.makeText(getActivity(), "რა არის პაროლი უკვე დაგავიწყდა?", Toast.LENGTH_LONG).show() }

        }




    }
}