package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        val controler1 =findNavController(R.id.nav_host_fragment)
        val controler = findNavController(R.id.Nav_Fragment)
        val appBarConfig = AppBarConfiguration(
            setOf(
                R.id.loginActivity,
                R.id.registerActivity,
                R.id.changePasswordActivity,
                R.id.recoverPasswordActivity,
                R.id.profileActivity,
                R.id.cartFragment,
                R.id.homeFragment



            )
        )
        setupActionBarWithNavController(controler,appBarConfig)
        navView.setupWithNavController(controler1)
    }
}